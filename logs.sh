#!/bin/bash

echo "getting logs for today"

cd storage/logs
NOW=$(date +"%Y-%m-%d")
tail -n 300 -f lumen-$NOW.log