@include('emails.layout.mailheader')   

<tr><td align="center"> <b>Transaction E-Reciept</b></td></tr>
<tr>
<td style='color: #000; background-color: #fff; ' colspan='3' align='left'>
    <center>
       
        <table cellpadding="5" cellspacing="5" style="font-size: 12px;">
            
            <tr>
                <td style="font-weight: bold;">Transaction Status</td><td> {{ $data['status'] }}. ( {{ $data['message'] }} )</td>
            </tr>
            <tr>
                <td style="font-weight: bold">Transaction SEQ</td><td> {{ $data['sequence'] }}</td>
            </tr>
            <tr>
                <td style="font-weight: bold">Amount</td><td> N {{ $data['amount'] }}</td>
            </tr>
            <tr>
                <td style="font-weight: bold">Wallet ID</td><td>{{ $data['wallet'] }}</td>
            </tr>
            <tr>
                <td style="font-weight: bold">Terminal ID</td><td>{{ $data['terminal'] }}</td>
            </tr>
            <tr>
                <td style="font-weight: bold">Reference</td><td>{{ $data['reference'] }}</td>
            </tr>
            <tr>
                <td style="font-weight: bold">Payment Method</td><td> {{ $data['paymentMethod'] }}</td>
            </tr>
            <tr>
                <td style="font-weight: bold">Customer Account</td><td> {{ $data['account'] }}</td>
            </tr>
            <tr>
                <td style="font-weight: bold">Service Type</td><td> {{ $data['serviceType'] }}</td>
            </tr>
            <tr>
                <td style="font-weight: bold">Token Generated</td><td> {{ is_array($data['token']) ? "NIL" : $data['token'] }}</td>
            </tr>
            <tr>
                <td style="font-weight: bold">Payment Channel</td><td> {{ $data['channel'] }}</td>
            </tr>
            <tr>
                <td style="font-weight: bold">Transaction Date</td><td> {{ $data['time'] }}</td>
            </tr>
        </table>
    </center>
</td>
</tr>
@include('emails.layout.mailfooter')   