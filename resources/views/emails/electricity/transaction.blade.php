@include('emails.layout.header')   
<table class="text-root" width="100%" cellpadding="0" cellspacing="0" style="border: none; margin: 0px; border-collapse: collapse; padding: 0px; width: 100%;">
<tbody valign="middle" style="border: none; margin: 0px; padding: 0px;">
    <tr valign="middle" style="border: none; margin: 0px; padding: 0px;">
        <td class="table-separator" width="6.25%" valign="middle" style="width: 6.25%; border: none; margin: 0px; padding: 0px;"></td>
        <td valign="middle" style="border: none; margin: 0px; padding: 0px;">
    <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0px; padding: 0px">
        <tbody>
            <tr>
                <td class="font text-paragraph" align="left" style="border: none; margin: 0px; padding: 0px 0px 5px; font-family: Circular, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: 200;
                            text-align: left; text-decoration: none; color: rgb(97, 100, 103); font-size: 14px; line-height: 20px;">
                    <table class="purchase-details" cellspacing="0" cellpadding="0" width="100%" bgcolor="#F7F7F7" style="margin: 0px; padding: 0px; background: rgb(247, 247, 247); border-collapse: collapse; width: 100%">
                        <tbody>
                            <tr>
                                <td style="border-style: solid; border-width: 1px; border-color: rgb(227, 227, 227)">
                                    <table cellspacing="0" cellpadding="0" width="100%" style="margin: 0px; padding: 0px; width: 100%">
                                        <tbody>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                            <tr>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                                <td valign="middle" style="font-weight: bold; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    Status
                                                </td>
                                                <td width="20%" valign="middle" style="text-align: right; width: 20%; vertical-align: top; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                   {{ $data['status'] }}
                                                </td>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                            </tr>
                                            <tr>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                            
                                            </tr>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-style: solid; border-width: 1px; border-color: rgb(227, 227, 227)">
                                    <table cellspacing="0" cellpadding="0" width="100%" style="margin: 0px; padding: 0px; width: 100%">
                                        <tbody>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                            <tr>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                                <td valign="middle" style="font-weight: bold; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    Sequence
                                                </td>
                                                <td width="20%" valign="middle" style="text-align: right; width: 20%; vertical-align: top; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    {{ $data['sequence'] }}
                                                </td>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                            </tr>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-style: solid; border-width: 1px; border-color: rgb(227, 227, 227)">
                                    <table cellspacing="0" cellpadding="0" width="100%" style="margin: 0px; padding: 0px; width: 100%">
                                        <tbody>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                            <tr>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                                <td valign="middle" style="font-weight: bold; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                Amount
                                                </td>
                                                <td width="20%" valign="middle" style="text-align: right; width: 20%; vertical-align: top; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    ₦{{ $data['amount'] }}
                                                </td>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                            </tr>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-style: solid; border-width: 1px; border-color: rgb(227, 227, 227)">
                                    <table cellspacing="0" cellpadding="0" width="100%" style="margin: 0px; padding: 0px; width: 100%">
                                        <tbody>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                            <tr>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                                <td valign="middle" style="font-weight: bold; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    Wallet ID
                                                </td>
                                                <td width="20%" valign="middle" style="text-align: right; width: 20%; vertical-align: top; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    {{ $data['wallet'] }}
                                                </td>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                            </tr>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-style: solid; border-width: 1px; border-color: rgb(227, 227, 227)">
                                    <table cellspacing="0" cellpadding="0" width="100%" style="margin: 0px; padding: 0px; width: 100%">
                                        <tbody>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                            <tr>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                                <td valign="middle" style="font-weight: bold; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    Terminal ID
                                                </td>
                                                <td width="20%" valign="middle" style="text-align: right; width: 20%; vertical-align: top; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    {{ $data['terminal'] }}
                                                </td>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                            </tr>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-style: solid; border-width: 1px; border-color: rgb(227, 227, 227)">
                                    <table cellspacing="0" cellpadding="0" width="100%" style="margin: 0px; padding: 0px; width: 100%">
                                        <tbody>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                            <tr>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                                <td valign="middle" style="font-weight: bold; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    Reference
                                                </td>
                                                <td width="20%" valign="middle" style="text-align: right; width: 20%; vertical-align: top; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    {{ $data['reference'] }}
                                                </td>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                            </tr>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-style: solid; border-width: 1px; border-color: rgb(227, 227, 227)">
                                    <table cellspacing="0" cellpadding="0" width="100%" style="margin: 0px; padding: 0px; width: 100%">
                                        <tbody>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                            <tr>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                                <td valign="middle" style="font-weight: bold; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    Payment Method
                                                </td>
                                                <td width="20%" valign="middle" style="text-align: right; width: 20%; vertical-align: top; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    {{ $data['paymentMethod'] }}
                                                </td>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                            </tr>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-style: solid; border-width: 1px; border-color: rgb(227, 227, 227)">
                                    <table cellspacing="0" cellpadding="0" width="100%" style="margin: 0px; padding: 0px; width: 100%">
                                        <tbody>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                            <tr>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                                <td valign="middle" style="font-weight: bold; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    Customer Account
                                                </td>
                                                <td width="20%" valign="middle" style="text-align: right; width: 20%; vertical-align: top; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    {{ $data['account'] }}
                                                </td>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                            </tr>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-style: solid; border-width: 1px; border-color: rgb(227, 227, 227)">
                                    <table cellspacing="0" cellpadding="0" width="100%" style="margin: 0px; padding: 0px; width: 100%">
                                        <tbody>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                            <tr>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                                <td valign="middle" style="font-weight: bold; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    Service Type
                                                </td>
                                                <td width="20%" valign="middle" style="text-align: right; width: 20%; vertical-align: top; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    {{ $data['serviceType'] }}
                                                </td>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                            </tr>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-style: solid; border-width: 1px; border-color: rgb(227, 227, 227)">
                                    <table cellspacing="0" cellpadding="0" width="100%" style="margin: 0px; padding: 0px; width: 100%">
                                        <tbody>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                            <tr>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                                <td valign="middle" style="font-weight: bold; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    Token Generated
                                                </td>
                                                <td width="20%" valign="middle" style="text-align: right; width: 20%; vertical-align: top; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    {{ is_array($data['token']) ? "NIL" : $data['token'] }}
                                                </td>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                            </tr>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-style: solid; border-width: 1px; border-color: rgb(227, 227, 227)">
                                    <table cellspacing="0" cellpadding="0" width="100%" style="margin: 0px; padding: 0px; width: 100%">
                                        <tbody>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                            <tr>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                                <td valign="middle" style="font-weight: bold; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    Payment Channel
                                                </td>
                                                <td width="20%" valign="middle" style="text-align: right; width: 20%; vertical-align: top; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    {{ $data['channel'] }}
                                                </td>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                            </tr>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-style: solid; border-width: 1px; border-color: rgb(227, 227, 227)">
                                    <table cellspacing="0" cellpadding="0" width="100%" style="margin: 0px; padding: 0px; width: 100%">
                                        <tbody>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                            <tr>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                                <td valign="middle" style="font-weight: bold; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    Date
                                                </td>
                                                <td width="20%" valign="middle" style="text-align: right; width: 20%; vertical-align: top; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                                                    {{ $data['time'] }}
                                                </td>
                                                <td width="5%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 5%;"></td>
                                            </tr>
                                            <tr class="purchase-details-padding" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px;">
                                                <td colspan="4" height="10" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 10px; font-size: 10px; line-height: 10px"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
        </td>
        <td class="table-separator" width="6.25%" valign="middle" style="width: 6.25%; border: none; margin: 0px; padding: 0px;"></td>
    </tr>
    <tr valign="middle" style="border: none; margin: 0px; padding: 0px;">
        <td colspan="3" class="text-padding" height="20" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 20px;"></td>
    </tr>
</tbody>
</table>
<table class="text-root" width="100%" cellpadding="0" cellspacing="0" style="border: none; margin: 0px; border-collapse: collapse; padding: 0px; width: 100%;">
<tbody valign="middle" style="border: none; margin: 0px; padding: 0px;">
    <tr valign="middle" style="border: none; margin: 0px; padding: 0px;">
        <td class="table-separator" width="6.25%" valign="middle" style="width: 6.25%; border: none; margin: 0px; padding: 0px;"></td>
        <td valign="middle" style="border: none; margin: 0px; padding: 0px;">

        </td>
        <td class="table-separator" width="6.25%" valign="middle" style="width: 6.25%; border: none; margin: 0px; padding: 0px;"></td>
    </tr>
    <tr valign="middle" style="border: none; margin: 0px; padding: 0px;">
        <td colspan="3" class="text-padding" height="20" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 20px;"></td>
    </tr>
</tbody>
</table>
<table class="text-root" width="100%" cellpadding="0" cellspacing="0" style="border: none; margin: 0px; border-collapse: collapse; padding: 0px; width: 100%;">
<tbody valign="middle" style="border: none; margin: 0px; padding: 0px;">
    <tr valign="middle" style="border: none; margin: 0px; padding: 0px;">
        <td class="table-separator" width="6.25%" valign="middle" style="width: 6.25%; border: none; margin: 0px; padding: 0px;"></td>
        <td valign="middle" style="border: none; margin: 0px; padding: 0px;">
        </td>
        <td class="table-separator" width="6.25%" valign="middle" style="width: 6.25%; border: none; margin: 0px; padding: 0px;"></td>
    </tr>
    <tr valign="middle" style="border: none; margin: 0px; padding: 0px;">
        <td colspan="3" class="text-padding" height="20" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 20px;"></td>
    </tr>
</tbody>
</table>
@include('emails.layout.footer') 