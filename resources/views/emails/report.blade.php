@include('emails.layout.mailheader')   
<tr>
<td style='color: #000; background-color: #fff;' colspan='3' align='left'>
    <p>
        {{ $Body }}
    </p>
    <p>
        <b>Flagging Criteria:</b> Transaction was declared <i> {{$data['status']}} </i> but was continously tried for <b><i>{{$freq}}</i></b> times.<br><br>
        <table cellpadding="5" cellspacing="5">
            
            <tr>
                <td>Transaction Status</td><td> {{ $data['status'] }}</td>
            </tr>
            <tr>
                <td>Wallet</td><td> {{ $data['wallet'] }}</td>
            </tr>
            <tr>
                <td>Product</td><td> {{ $data['product'] }}</td>
            </tr>
            <tr>
                <td>Amount</td><td> &#8358;{{ $data['amount']/100 }}</td>
            </tr>
            <tr>
                <td>Vas Customer Name</td><td> {{ $data['VASCustomerName'] ?? "NIL"}}</td>
            </tr>
            <tr>
                <td>Vas Customer Phone</td><td>{{ $data['VASCustomerPhone'] ?? "NIL"}}</td>
            </tr>
            <tr>
                <td>Vas Customer Account</td><td> {{ $data['VASCustomerAccount'] ?? "NIL"}}</td>
            </tr>
            <tr>
                <td>Vas Customer Address</td><td>{{ $data['VASCustomerAddress'] ?? "NIL"}}</td>
            </tr>
            <tr>
                <td>Vas Biller Name</td><td>{{ $data['VASBillerName'] ?? "NIL"}}</td>
            </tr>
            <tr>
                <td>Vas Provider Name</td><td>{{ $data['VASProviderName'] ?? "NIL"}}</td>
            </tr>
            <tr>
                <td>Terminal</td><td>{{ $data['terminal'] ?? "NIL"}}</td>
            </tr>
            <tr>
                <td>Channel</td><td>;{{ $data['channel'] ?? "NIL"}}</td>
            </tr>
            <tr>
                <td>Category</td><td>{{ $data['category'] ?? "NIL"}}</td>
            </tr>
            <tr>
                <td>Description</td><td>{{ $data['description'] ?? "NIL"}}</td>
            </tr>
            <tr>
                <td>Reference</td><td>Reference: {{ $data['reference'] ?? "NIL"}},  Sequence: {{ $data['id'] ?? "NIL"}}</td>
            </tr>
            <tr>
                <td>Payment Method</td><td>{{ $data['paymentMethod'] ?? "NIL"}}</td>
            </tr>
            <tr>
                <td>Location of Transaction</td><td>{{ $data['location'] ?? "NIL"}}</td>
            </tr>
            <tr>
                <td>Reason</td><td>{{ $data['reason'] ?? "NIL"}}, Message: {{ $data['message'] ?? "NIL"}}</td>
            </tr>
            
            <tr>
                <td>Date/Time Flagged</td><td>{{ date("jS \of F Y, h:i:s A") }}</td>
            </tr>
            
        </table>
    </p>
</td>
</tr>
@include('emails.layout.mailfooter')   