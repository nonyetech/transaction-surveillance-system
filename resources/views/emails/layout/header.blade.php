<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style type="text/css">
        @media only screen and (min-device-width: 481px) {
            div[id="main"] {
                width: 480px !important;
            }
        }
	table{
    font-family: Circular, 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-weight: 200;
    text-decoration: none;
    color: rgb(97, 100, 103)!important;
    font-size: 11px;
    line-height: 20px;}
    </style>
    <!--[if mso]><style>body, table, tr, td, h1, h2, h3, h4, h5, h6, center p, a, span, table.MsoNormalTable {font-family: "Helvetica Neue", "Helvetica", "Arial", sans-serif !important}</style><![endif]-->
</head>

<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0" marginheight="0" marginwidth="0" style="-webkit-font-smoothing: antialiased; width: 100% !important; -webkit-text-size-adjust: none; margin: 0; padding: 0">
    
    <!--TEST-FORM-->
    <!--[if (mso) | (IE)]><table cellpadding="0" cellspacing="0" border="0" valign="top" width="480" align="center"><tr><td valign="top" align="left" style=" word-break:normal; border-collapse:collapse; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:12px; line-height:18px; color:#555555;"><![endif]-->
<table cellpadding="0" cellspacing="0" border="0" valign="top" width="100%" align="center" style=" width:100%; max-width:580px; font-size: 12px !important;">
<tbody><tr>
<td valign="top" align="left" style=" word-break:normal; border-collapse:collapse; font-family:'Circular', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:12px; line-height:18px; color:#555555;">
<center>
    <div id="main">
        <table class="header-root" width="100%" height="50" cellpadding="0" cellspacing="0" style="border: none; margin: 0px; border-collapse: collapse; padding: 0px; width: 100%; height: 30px; font-size: 11px !important">
            <tbody valign="middle" style="border: none; margin: 0px; padding: 0px;">
                <tr height="20" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 20px;">
                    <td colspan="3" height="20" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 20px;"></td>
                </tr>
                <tr valign="middle" style="border: none; margin: 0px; padding: 0px;">
                    <td width="6.25%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 6.25%;"></td>
                    <td valign="middle" style="border: none; margin: 0px; padding: 0px;">
                        <a href="http://www.payvice.com/" style="border: none; display:none; margin: 0px; padding: 0px; text-decoration: none;" target="_blank"><img class="logo" src="http://iisysgroup.com/assets-img/logo-payvice.png" width="170" height="57" alt="" style="border: none; margin: 0px; padding: 0px; margin:auto; display: block; max-width: 100%; width: 170px; height: 57px;"></a>
                        <a href="http://www.payvice.com/" style="border: none; margin: 0px; padding: 0px; text-decoration: none;" target="_blank"><img class="logo" src="http://iisysgroup.com/assets-img/logo-payvice.png" width="170" height="57" alt="" style="border: none; margin: 0px; padding: 0px; margin:auto; display: block; max-width: 100%; width: 170px; height: 57px;"></a>
                    </td>
                    <td width="6.25%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 6.25%;"></td>
                </tr>
                <tr height="20" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 20px;">
                    <td colspan="3" height="20" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 20px;"></td>
                </tr>
            </tbody>
        </table>
        <table class="title-subtitle-root" width="100%" cellpadding="0" cellspacing="0" style="border: none; margin: 0px; border-collapse: collapse; padding: 0px; width: 100%;">
            <tbody valign="middle" style="border: none; margin: 0px; padding: 0px;">
                <tr height="28" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 28px;">
                    <td colspan="3" height="28" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 28px;"></td>
                </tr>
                <tr valign="middle" style="border: none; margin: 0px; padding: 0px;">
                    <td width="6.25%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 6.25%;"></td>
                    <td valign="middle" style="border: none; margin: 0px; padding: 0px;">
                            <h1 class="font title-subtitle-title" align="center" style="border: none; margin: 0px; padding: 0px; font-family: Circular, 'Helvetica Neue', Helvetica, Arial, sans-serif; text-decoration: none; color: rgb(85, 85, 85); font-size: 30px; font-weight: bold;          line-height: 45px; letter-spacing: -0.04em; text-align: center;">
                                Transaction Receipt
                            </h1>

                    </td>
                    <td width="6.25%" valign="middle" style="border: none; margin: 0px; padding: 0px; width: 6.25%;"></td>
                </tr>
                <tr height="16" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 16px;">
                    <td colspan="3" height="16" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 16px;"></td>
                </tr>
            </tbody>
        </table>
        <table class="text-root" width="100%" cellpadding="0" cellspacing="0" style="border: none; margin: 0px; border-collapse: collapse; padding: 0px; width: 100%;">
            <tbody valign="middle" style="border: none; margin: 0px; padding: 0px;">
                <tr valign="middle" style="border: none; margin: 0px; padding: 0px;">
                    <td class="table-separator" width="6.25%" valign="middle" style="width: 6.25%; border: none; margin: 0px; padding: 0px;"></td>
                    <td valign="middle" style="border: none; margin: 0px; padding: 0px;">
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0px; padding: 0px">
                            <tbody>
                                <tr>
                                    <td class="font text-paragraph" align="left" style="border: none; margin: 0px; padding: 0px 0px 5px; font-family: Circular, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: 200; text-align: left; text-decoration: none; color: rgb(97, 100, 103); font-size: 14px; line-height: 20px;">
                                        <center style="border: none; margin: 0px; padding: 0px;">
                                            
                                            <center style="border: none; margin: 0px; padding: 0px;"></center>
                                        </center>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0px; padding: 0px">
                            <tbody>
                                <tr>
                                    <td class="font text-paragraph" align="left" style="border: none; margin: 0px; padding: 0px 0px 5px; font-family: Circular, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: 200; text-align: left; text-decoration: none; color: rgb(97, 100, 103); font-size: 14px; line-height: 20px;">
                                        <center style="border: none; margin: 0px; padding: 0px;">
                                            
                                            Dear Customer, Thank you for using Payvice. 

                                            <center style="border: none; margin: 0px; padding: 0px;"></center>
                                        </center>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td class="table-separator" width="6.25%" valign="middle" style="width: 6.25%; border: none; margin: 0px; padding: 0px;"></td>
                </tr>
                <tr valign="middle" style="border: none; margin: 0px; padding: 0px;">
                    <td colspan="3" class="text-padding" height="20" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 20px;"></td>
                </tr>
            </tbody>
        </table>