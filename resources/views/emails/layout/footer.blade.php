<table class="text-root" width="100%" cellpadding="0" cellspacing="0" style="border: none; margin: 0px; border-collapse: collapse; padding: 0px; width: 100%;">
    <tbody valign="middle" style="border: none; margin: 0px; padding: 0px;">
        <tr valign="middle" style="border: none; margin: 0px; padding: 0px;">
            <td class="table-separator" width="6.25%" valign="middle" style="width: 6.25%; border: none; margin: 0px; padding: 0px;"></td>
            <td valign="middle" style="border: none; margin: 0px; padding: 0px;">
                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: 0px; padding: 0px">
                    <tbody>
                        <tr>
                            <td class="font text-paragraph" align="left" style="border: none; margin: 0px; padding: 0px 0px 5px; font-family: Circular, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: 200; text-align: left; text-decoration: none; color: rgba(109, 109, 109,.5); font-size: 14px; line-height: 20px;">
                                <center style="border: none; margin: 0px; padding: 0px;">
                                    <font size="-1" style="border: none; margin: 0px; padding: 0px;">
This email was generated for {{ $to }} and will not be published or shared as part of our data privacy policies.</font></center>
                            </td>
                        </tr>
                                    <tr>
                            <td class="font text-paragraph" align="left" style="border: none; margin: 0px; padding: 0px 0px 5px; font-family: Circular, 'Helvetica Neue', Helvetica, Arial, sans-serif; font-size:10px; font-weight: 200; text-align: left; text-decoration: none; color: rgb(97, 100, 103); line-height: 20px;">
                                <center style="border: none; margin: 0px; padding: 0px;">
                                    <font size="-1" style="border: none; margin: 0px; padding: 0px;">
© {{ date("Y") }} Itex Integrated Services Ltd.</font></center>
                            </td>
                        </tr>
                        
                    </tbody>
                </table>
            </td>
            <td class="table-separator" width="6.25%" valign="middle" style="width: 6.25%; border: none; margin: 0px; padding: 0px;"></td>
        </tr>
        <tr valign="middle" style="border: none; margin: 0px; padding: 0px;">
            <td colspan="3" class="text-padding" height="20" valign="middle" style="border: none; margin: 0px; padding: 0px; height: 20px;"></td>
        </tr>
    </tbody>
</table>
<style style="border: none; margin: 0px; padding: 0px;">
    .footer-root a.footer-link {
        color: #6D6D6D;
        text-decoration: none;
        font-weight: bold;
    }
</style>
</div>
</center>
</td>
</tr>
</tbody></table>
<!--[if (mso) | (IE)]></td></tr></table><![endif]-->




</body>