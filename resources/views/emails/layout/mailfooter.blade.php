<tr>
    <td align="center"><img src="https://res.cloudinary.com/ewebstech/image/upload/v1555870514/ITEX/ptspsignature.jpg" style="text-align: center" /> </td>
</tr>
<tr>
    <td style="text-align: center; font-size: 10px;">This email was sent to <u>{{ $to }}</u>. If you recieved this email in error, <br />please delete immediately and report this incidence to ITEX INTEGRATED SERVICES LTD. <br />To stop receiving emails from this source, you can <a href="#">unsubscribe</a><br> Copyright 2019 ITEX Integrated Services Ltd. All Rights Reserved.
    </td>
</tr>
</table>
</center>	
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>