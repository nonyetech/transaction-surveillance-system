<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;


class PushMails extends Controller
{
    
    private function mailTemplate($template,$data)
    {
        $contents = view('emails.'.$template, $data)->render();
        return $contents;
    }

    public function sendMail($params){
        $mail = new PHPMailer(true);
        $subject = $params['Subject'];
        $toAddress = $params['to'];
        $mailTemplate = $this->mailTemplate($params["template"],$params);

        $fromAddress = "i-alert@iisysgroup.com";
        $replyTo = "vassupport@iisysgroup.com";
       
        try{
            $mail->ClearAddresses();
            $mail->ClearAllRecipients();

            $mail->isSMTP();
            $mail->SMTPDebug = 0;       // Enable verbose debug output | Production Server = 0
            $mail->CharSet = 'utf-8';
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = "tls";
            
            // $mail->Host = "email-smtp.eu-west-1.amazonaws.com"; //gmail has host > smtp.gmail.com
            // $mail->Port = "587"; //gmail has port > 587 . without double quotes
            // $mail->Username = "AKIAVGIHDBGPKLDTCX5F"; //your username. actually your email
            // $mail->Password = "BPp74v36fVsq2HoK0G2Wl3QxUbCeIdPQyhWGCKjGLaO7"; // your password. your mail password

            $mail->Host = "email-smtp.us-west-2.amazonaws.com"; //gmail has host > smtp.gmail.com
            $mail->Port = "587"; //gmail has port > 587 . without double quotes
            $mail->Username = "AKIAVGIHDBGPK6RM2PKP"; //your username. actually your email
            $mail->Password = "BNXM/4mMjqzhOwC9lL7J89XBn5olbxvIH0RiXDeOBUu4"; // your password. your mail password

                      
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );

            $mail->setFrom($fromAddress, "Payvice Receipts"); 
            $mail->Subject = $subject;
            $mail->MsgHTML($mailTemplate);
            $mail->addAddress($toAddress , 'Payvice Receipts');

            $mail->addReplyTo($fromAddress, "Itex Integrated Services Ltd");

            if ($mail->send()) {
                $mail->ClearAddresses();
                $mail->ClearAllRecipients();
                return true;
            } else {
                $mail->ClearAddresses();
                $mail->ClearAllRecipients();
                return false;
            }
        }catch(Exception $e){
            $mail->ClearAddresses();
            $mail->ClearAllRecipients();
            \Log::info($e->getMessage());
            \Log::error("Message could not be sent, ".  json_encode($mail->ErrorInfo));
            return $e->getMessage();
        }
       
    }

}