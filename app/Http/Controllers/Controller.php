<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use App\Helpers\Response;
use App\Helpers\generator;
use App\Helpers\customCode;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use Response, generator;

    protected function responseToArray($response){
        $fileContents = str_replace(array("\n", "\r", "\t"), '', $response->getBody()->getContents());
        $fileContents = trim(str_replace('"', "'", $fileContents));
        $fileContents = str_replace("'",'"', $fileContents);
        $phpArray = json_decode($fileContents, true);
        return $phpArray;
    }

    protected function arrayToString($array){
        $string = implode("|",$array);
        return $string;
    }

}
