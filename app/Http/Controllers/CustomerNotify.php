<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\PushMails;
use App\Http\Controllers\HttpRequests;
use App\Http\Controllers\VasController;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Client as GuzzleClient;


class CustomerNotify extends Controller
{

    public function __construct()
    {
        $this->mail = new PushMails;
        $this->client = new GuzzleClient();
    }

    public function getDataOnVas(){
        
        try{
            $response = $this->client->request('GET', HttpRequests::baseURI().'/vas/get-vas-transactions-to-mail', [
                'headers' => null
            ]);

            $responseData = $this->responseToArray($response);

            \Log::info("Raw Response from Server: " . json_encode($responseData));
        
            if($responseData){
                $i = 0;
                foreach($responseData as $data){
                    $Response[$i] = json_decode($data['metadata'], true);

                    $location = $Response[$i]['location'];
                    $Response[$i]['location'] =$Response[$i]['ip_address']  . " | " . $location['countryCode'] . " | " . $location["cityName"] . " | " . $location["regionName"] . " | Long: " . $location["longitude"] . " | Lat: " . $location["latitude"];

                    $Response[$i]['created_at'] = $data['created_at'];
                    $Response[$i]['updated_at'] = $data['updated_at'];
                    $i++;
                }

                \Log::info("Response gotten from VAS server, Pulled records successfully");
                $fp = fopen('customers.json', 'w');
                fwrite($fp, json_encode($Response));
                fclose($fp);
            }
            return true;
           
        } catch (RequestException | ConnectException | BaseException $e) {
            if ($e->hasResponse()) {
                $errorMsg = $this->jsonToArray($e->getResponse()->getBody()->getContents());
                return $this->exceptionError($errorMsg, HttpStatusCodes::OK);
            }
        } 
        
    }
   
    public function prepareNotificationParams(){
         // Get Data from File
        if(file_exists("customers.json")){
            $jsonFile = file_get_contents("customers.json");
            $responseAllData = json_decode($jsonFile, true);
            //dd($responseData);
        } else {
            \Log::debug("No Data Found");
            return "No Data Available";
        }
        //dd($responseData);
        $i = 0;

        foreach($responseAllData as $data){
            $isEmail = $this->checkEmail($data['username']);
            if($isEmail){
                $data['username'] = strtolower($data['username']);
                //$data['username'] = "ewebstech@gmail.com";

                if($data['product'] == "WITHDRAWAL"){

                    $dir = "notifs".date("Y-m-d")."".$data['category'];
                    !is_dir($dir) ? mkdir($dir) : ''; 

                    $mailData[$i]['status'] = "WITHDRAWAL ". strtoupper($data['status']); 
                    $mailData[$i]['amount'] = number_format($data['nairaAmount']); 
                    $mailData[$i]['message'] = $data['message'] ?? '';
                    $mailData[$i]['sequence'] = $data['sequence'] ?? '';
                    $mailData[$i]['serviceType'] = $data['name'] ?? '';
                    $mailData[$i]['wallet'] = $data['wallet'] ?? '';
                    $mailData[$i]['terminal'] = $data['terminal'] ?? '';
                    $mailData[$i]['time'] = $data['updated_at'] ?? ''; 
                    $mailData[$i]['channel'] = $data['channel'] ?? ''; 
                    $mailData[$i]['amountSettled'] = number_format($data['productCodeData']['amountSettled']/100);
                    $mailData[$i]['reference'] = $data['reference'] ?? '';
                    $mailData[$i]['paymentMethod'] = $data['paymentMethod'] ?? '';
                    $mailData[$i]['account'] = $data['VASCustomerAccount'] ?? '';

                    if(file_exists($dir."/sentnotifications.json")){
                        $jsonFile = file_get_contents($dir."/sentnotifications.json");
                        $responseData = json_decode($jsonFile, true);
                        foreach($responseData as $resp){
                            if($resp['id'] == $data['id'] && $resp['username'] == $data['username']){
                                $searchedValue = true;
                                break;
                            }
                        }
                    } else {
                        $searchedValue = false;
                    }

                    !isset($searchedValue) ? $searchedValue = false : '';

                    //dd($searchedValue);
                     // Verify if there exists an email in this payload
                    $isEmail = $this->checkEmail($data['username']);
                    
                    if(isset($data['username']) and $isEmail === true and $mailData[$i]['status'] !== "INITIALIZED" and $searchedValue === false){
                        try{
                            $mailParams = [
                                'Subject' => 'Payvice '. strtoupper($data['product']) .' Transaction Notification',
                                'template' => 'vicebanking.withdrawal',
                                'data' => $mailData[$i],
                                'to' => $data['username']
                            ];
                            
                            $send_mail = $this->mail->sendMail($mailParams);
                            //dd($send_mail);
                            if($send_mail === true){
                                \Log::info("Notification Sent: ==== " . print_r($mailParams, true));
                                // Register Notifications
                                $sentNotifications[$i] = [
                                    'id' => $data['id'],
                                    'username' => $data['username']    
                                ];

                                $fp = fopen($dir.'/sentnotifications.json', 'w');
                                fwrite($fp, json_encode($sentNotifications));
                                fclose($fp);
                                
                            } else {
                                \Log::info("Error: Could not send Notification: ==== " . print_r($mailParams, true));
                            }
                           
                        } catch(BaseException $e){
                            \Log::debug($e->getMessage());
                        }
                    }

                    } elseif($data['product'] == "TRANSFER"){

                        $dir = "notifs".date("Y-m-d")."".$data['category'];
                        !is_dir($dir) ? mkdir($dir) : ''; 

                        $mailData[$i]['status'] = strtoupper($data['status']); 
                        $mailData[$i]['message'] = $data['message'] ?? '';
                        $mailData[$i]['amount'] = number_format($data['nairaAmount']); 
                        $mailData[$i]['sequence'] = $data['sequence'] ?? ''; 
                        $mailData[$i]['serviceType'] = $data['name'] ?? '';
                        $mailData[$i]['wallet'] = $data['wallet'] ?? '';
                        $mailData[$i]['terminal'] = $data['terminal'] ?? '';
                        $mailData[$i]['time'] = $data['updated_at'] ?? ''; 
                        $mailData[$i]['channel'] = $data['channel'] ?? ''; 
                        $mailData[$i]['bankName'] = $data['productCodeData']['bankName'] ?? '';
                        $mailData[$i]['charge'] = number_format(($data['productCodeData']['convenienceFee']/100));
                        $mailData[$i]['reference'] = $data['reference'] ?? '';
                        $mailData[$i]['paymentMethod'] = $data['paymentMethod'] ?? '';
                        $mailData[$i]['beneficiary'] = $data['VASCustomerAccount'] ?? '';

                        if(file_exists($dir."/sentnotifications.json")){
                            $jsonFile = file_get_contents($dir."/sentnotifications.json");
                            $responseData = json_decode($jsonFile, true);
                            foreach($responseData as $resp){
                                if($resp['id'] == $data['id'] && $resp['username'] == $data['username']){
                                    $searchedValue = true;
                                    break;
                                }
                            }
                        } else {
                            $searchedValue = false;
                        }
                        
                        !isset($searchedValue) ? $searchedValue = false : '';
        
                         // Verify if there exists an email in this payload
                        $isEmail = $this->checkEmail($data['username']);
                        
                        if(isset($data['username']) and $isEmail === true and $mailData[$i]['status'] !== "INITIALIZED" and $searchedValue === false){
                            try{
                                $mailParams = [
                                    'Subject' => 'Payvice '. strtoupper($data['product']) .' Transaction Notification',
                                    'template' => 'vicebanking.transfer',
                                    'data' => $mailData[$i],
                                    'to' => $data['username']
                                ];
                                
                                $send_mail = $this->mail->sendMail($mailParams);
                                //dd($send_mail);
                                if($send_mail === true){
                                    \Log::info("Notification Sent: ==== " . print_r($mailParams, true));
                                      // Register Notifications
                                    $sentNotifications[$i] = [
                                        'id' => $data['id'],
                                        'username' => $data['username']    
                                    ];

                                    $fp = fopen($dir.'/sentnotifications.json', 'w');
                                    fwrite($fp, json_encode($sentNotifications));
                                    fclose($fp);
                                } else {
                                    \Log::info("Error: Could not send Notification: ==== " . print_r($mailParams, true));
                                }
                            
                            } catch(BaseException $e){
                                \Log::debug($e->getMessage());
                            }
                        }

                    } elseif($data['category'] == "Electricity"){

                        $dir = "notifs".date("Y-m-d")."".$data['category'];
                        !is_dir($dir) ? mkdir($dir) : ''; 
                    
                        $mailData[$i]['status'] = strtoupper($data['status']); 
                        $mailData[$i]['sequence'] = $data['sequence']; 
                        $mailData[$i]['message'] = $data['message'] ?? '';
                        $mailData[$i]['amount'] = number_format($data['nairaAmount']); 
                        $mailData[$i]['serviceType'] = $data['name'] ?? '';
                        $mailData[$i]['wallet'] = $data['wallet'] ?? '';
                        $mailData[$i]['terminal'] = $data['terminal'] ?? '';
                        $mailData[$i]['time'] = $data['updated_at'] ?? ''; 
                        $mailData[$i]['channel'] = $data['channel'] ?? ''; 
                        $mailData[$i]['reference'] = $data['reference'] ?? '';
                        $mailData[$i]['paymentMethod'] = strtoupper($data['paymentMethod']);
                        $mailData[$i]['account'] = $data['VASCustomerAccount'] ?? '';
                        $tokenResponse = isset($data['response']) ? (array) json_decode($data['response'], true) : $data['token'] ?? '';
                        $mailData[$i]['token'] = $tokenResponse['token'] ?? $tokenResponse ?? '';
                        
                        if(file_exists($dir."/sentnotifications.json")){
                            $jsonFile = file_get_contents($dir."/sentnotifications.json");
                            $responseData = json_decode($jsonFile, true);
                            foreach($responseData as $resp){
                                if($resp['id'] == $data['id'] && $resp['username'] == $data['username']){
                                    $searchedValue = true;
                                    break;
                                }
                            }
                        } else {
                            $searchedValue = false;
                        }
                        
                        !isset($searchedValue) ? $searchedValue = false : '';

                         // Verify if there exists an email in this payload
                        $isEmail = $this->checkEmail($data['username']);
                        
                        if(isset($data['username']) and $isEmail === true and $mailData[$i]['status'] !== "INITIALIZED" and $searchedValue === false){
                            try{
                                $mailParams = [
                                    'Subject' => 'Payvice '. strtoupper($data['product']) .' Transaction Notification',
                                    'template' => 'electricity.transaction',
                                    'data' => $mailData[$i],
                                    'to' => $data['username']
                                ];
                                
                                $send_mail = $this->mail->sendMail($mailParams);
                                //dd($send_mail);
                                if($send_mail === true){
                                    \Log::info("Notification Sent: ==== " . print_r($mailParams, true));
                                      // Register Notifications
                                    $sentNotifications[$i] = [
                                        'id' => $data['id'],
                                        'username' => $data['username']    
                                    ];

                                    $fp = fopen($dir.'/sentnotifications.json', 'w');
                                    fwrite($fp, json_encode($sentNotifications));
                                    fclose($fp);

                                } else {
                                    \Log::info("Error: Could not send Notification: ==== " . print_r($mailParams, true));
                                }
                            
                            } catch(BaseException $e){
                                \Log::debug($e->getMessage());
                            }
                        }
                
                    } elseif($data['category'] == "TV"){
                        
                        $dir = "notifs".date("Y-m-d")."".$data['category'];
                        !is_dir($dir) ? mkdir($dir) : ''; 

                        $mailData[$i]['status'] = strtoupper($data['status']); 
                        $mailData[$i]['sequence'] = $data['sequence'] ?? ''; 
                        $mailData[$i]['message'] = $data['message'] ?? '';
                        $mailData[$i]['amount'] = number_format($data['nairaAmount']); 
                        $mailData[$i]['wallet'] = $data['wallet'] ?? '';
                        $mailData[$i]['terminal'] = $data['terminal'] ?? '';
                        $mailData[$i]['time'] = $data['updated_at'] ?? ''; 
                        $mailData[$i]['channel'] = $data['channel'] ?? ''; 
                        $mailData[$i]['reference'] = $data['reference'] ?? '';
                        $mailData[$i]['paymentMethod'] = strtoupper($data['paymentMethod']);
                        $mailData[$i]['account'] = $data['VASCustomerAccount'] ?? '';

                        if(file_exists($dir."/sentnotifications.json")){
                            $jsonFile = file_get_contents($dir."/sentnotifications.json");
                            $responseData = json_decode($jsonFile, true);
                            foreach($responseData as $resp){
                                if($resp['id'] == $data['id'] && $resp['username'] == $data['username']){
                                    $searchedValue = true;
                                    break;
                                } 
                            }
                        } else {
                            $searchedValue = false;
                        }
        
                        !isset($searchedValue) ? $searchedValue = false : '';
                         // Verify if there exists an email in this payload
                        $isEmail = $this->checkEmail($data['username']);
       
                        if(isset($data['username']) and $isEmail === true and $mailData[$i]['status'] !== "INITIALIZED" and $searchedValue === false){
                            try{
                                $mailParams = [
                                    'Subject' => 'Payvice '. strtoupper($data['product']) .' Transaction Notification',
                                    'template' => 'tv.tv',
                                    'data' => $mailData[$i],
                                    'to' => $data['username']
                                ];
                                
                                $send_mail = $this->mail->sendMail($mailParams);
                                //dd($send_mail);
                                if($send_mail === true){
                                    \Log::info("Notification Sent: ==== " . print_r($mailParams, true));
                                      // Register Notifications
                                    $sentNotifications[$i] = [
                                        'id' => $data['id'],
                                        'username' => $data['username']    
                                    ];

                                    $fp = fopen($dir.'/sentnotifications.json', 'w');
                                    fwrite($fp, json_encode($sentNotifications));
                                    fclose($fp);

                                } else {
                                    \Log::info("Error: Could not send Notification: ==== " . print_r($mailParams, true));
                                }
                            
                            } catch(BaseException $e){
                                \Log::debug($e->getMessage());
                            }
                        }
                        
                    }

                }
            
            $i++;

        }
        
        return true;
        
    }

    private function checkEmail($email) {
        $find1 = strpos($email, '@');
        $find2 = strpos($email, '.');
        return ($find1 !== false && $find2 !== false && $find2 > $find1);
    }
   


}