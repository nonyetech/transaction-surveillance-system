<?php

namespace App\Http\Controllers;

use App\Utils\Rules;
use Illuminate\Http\Request;
use App\Http\Controllers\HttpRequests;
use GuzzleHttp\Client as GuzzleClient;
use App\Http\Controllers\MailController;
use GuzzleHttp\Exception\ConnectException;

class VasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new GuzzleClient();
        $this->mail = new MailController;
    }

    public function getDataOnVas(){
        
        try{
            $response = $this->client->request('GET', HttpRequests::baseURI().'/vas/get-vas-transactions', [
                'headers' => null
            ]);

            $responseData = $this->responseToArray($response);

            \Log::info("Raw Response from Server: " . json_encode($responseData));
        
            if($responseData){
                $i = 0;
                foreach($responseData as $data){
                    $Response[$i] = json_decode($data['metadata'], true);

                    $location = $Response[$i]['location'];
                    $Response[$i]['location'] =$Response[$i]['ip_address']  . " | " . $location['countryCode'] . " | " . $location["cityName"] . " | " . $location["regionName"] . " | Long: " . $location["longitude"] . " | Lat: " . $location["latitude"];

                    $Response[$i]['created_at'] = $data['created_at'];
                    $Response[$i]['updated_at'] = $data['updated_at'];
                    $i++;
                }

                \Log::info("Response gotten from VAS server, Pulled records successfully");
                $fp = fopen('results.json', 'w');
                fwrite($fp, json_encode($Response));
                fclose($fp);
            }
            return true;
           
        } catch (RequestException | ConnectException | BaseException $e) {
            if ($e->hasResponse()) {
                $errorMsg = $this->jsonToArray($e->getResponse()->getBody()->getContents());
                return $this->exceptionError($errorMsg, HttpStatusCodes::OK);
            }
        } 
        
    }

    public function validateFailedTransactions(){
        // Perform Validations
        if(file_exists("results.json")){
            $jsonFile = file_get_contents("results.json");
            $responseData = json_decode($jsonFile, true);
            //dd($responseData);
        } else {
            \Log::debug("No Data Found");
            return "No Data Available";
        }
        
        //dd($responseData);
        foreach($responseData as $current_key => $current_array){
            $i = 0;
            $hijackData[$i] = $current_array['id'];
            foreach ($responseData as $search_key => $search_array) {
                if($search_key != $current_key){
                    if(isset($current_array['reason']) and isset($current_array['category']) and isset($current_array['paymentMethod'])){
                        $fftCount = Rules::fraudulentFailedTransactions($current_key, $current_array, $search_key, $search_array);
                        if($fftCount == true){
                            $i++;
                            $hijackData[$i] = $search_array['id'];
                            $walletData = $search_array;
                            \Log::debug("$i - Duplicate Failed Transaction Found");
                        }  
                    } 
                }
            }
        }   

        $Tran_Freq = count($hijackData);
        $No_Of_Trials = Rules::getRule('FAILED_TRANSACTION_SETTINGS')['Trials'];
        
        if($Tran_Freq > $No_Of_Trials){
            \Log::info("Malicious Transaction Found: The same failed transaction from wallet has been tried for more than $No_Of_Trials times");
            // Send Notifications
            try{
                $mailParams = [
                    'Subject' => 'TMS Report: Wallet Multiple Failed Transactions',
                    'Body' => 'During scheduled transaction monitoring the system has flagged Wallet ID: '. $walletData['wallet'] . '. Please investigate further.',
                    'template' => 'report',
                    'data' => $walletData,
                    'freq' => $Tran_Freq
                ];
                
                $send_mail = $this->mail->sendMail($mailParams);
                if($send_mail == true){
                    \Log::info("Nice: Notification sent accordingly");
                } else {
                    \Log::info("Error: Could not send Notification ");
                }
               
            } catch(BaseException $e){
                \Log::debug($e->getMessage());
            }
            

        }

    }


    public function validateSuccessfulTransactions(){
        // Perform Validations
        if(file_exists("results.json")){
            $jsonFile = file_get_contents("results.json");
            $responseData = json_decode($jsonFile, true);
        } else {
            \Log::debug("No Data Found");
            return "No Data Available";
        }
        //dd($responseData);
        foreach($responseData as $current_key => $current_array){
            $i = 0;
            $hijackData[$i] = $current_array['id'];
            foreach ($responseData as $search_key => $search_array) {
                if($search_key != $current_key){
                    if(isset($current_array['reason']) and isset($current_array['category']) and isset($current_array['paymentMethod'])){
                        $fftCount = Rules::fraudulentSuccessfulTransactions($current_key, $current_array, $search_key, $search_array);
                        if($fftCount == true){
                            $i++;
                            $hijackData[$i] = $search_array['id'];
                            $walletData = $search_array;
                            \Log::debug("$i - Duplicate Successful Transaction Found");
                        }   
                    }
                }
            }
        }   

        $Tran_Freq = count($hijackData);
        $No_Of_Trials = Rules::getRule('SUCCESSFUL_TRANSACTION_SETTINGS')['Trials'];
        
        if($Tran_Freq > $No_Of_Trials){
            \Log::info("Malicious Transaction Found: Successful transactions from a wallet has been done for more than $No_Of_Trials times");
            // Send Notifications
            try{
                $mailParams = [
                    'Subject' => 'TSS Report: Wallet Multiple Successful Transactions',
                    'Body' => 'During scheduled transaction monitoring, the system has flagged Wallet ID: '. $walletData['wallet'] . '. Please investigate further.',
                    'template' => 'report',
                    'data' => $walletData,
                    'freq' => $Tran_Freq
                ];
                
                $send_mail = $this->mail->sendMail($mailParams);
                if($send_mail == true){
                    \Log::info("Nice: Notification sent accordingly");
                } else {
                    \Log::info("Error: Could not send Notification ");
                }
               
            } catch(BaseException $e){
                \Log::debug($e->getMessage());
            }
            

        }

    }


}
