<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Closure;

class BvnLockDown
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $rawRequestBody = $request->getContent();

        $signatureHeader = $request->header('ITEX-Signature');
        $nonceHeader = $request->header('ITEX-Nonce');
        $contentType = $request->header('Content-Type');

        $secret = env("ITEX_BVN_SECRET");   

        $realSignature = strtolower(hash('sha256', $nonceHeader . urlencode(base64_encode($secret)))); 

        if(strtolower($signatureHeader) == $realSignature && $contentType == "application/json"){

            return $next($request);

        }

        $response['status'] = false;
        $response['message'] = "Invalid Request Signature";

        if(env("APP_ENV") == "local"){
            //See Secret, do not remove comment
            //$response['ref'] = uniqid() . '---' . $realSignature;
        }

        return response()->json([$response],403);
        
    }
}
