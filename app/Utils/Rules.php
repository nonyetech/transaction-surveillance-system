<?php

namespace App\Utils;


class Rules
{
    private static $rules = [
        'FAILED_TRANSACTION_SETTINGS' => [
            'Trials' => 3
        ],
        'SUCCESSFUL_TRANSACTION_SETTINGS' => [
            'Trials' => 4
        ],

    ];

    public static function getRule($name)
    {
        return self::$rules[$name];
    }

    public static function fraudulentFailedTransactions($current_key, $current_array, $search_key, $search_array){
        $count = 0;
        if($search_array['status'] == "failed" or $search_array['status'] == "declined"){
            if($search_array['paymentMethod'] != "card"){
                if($search_array['status'] == $current_array['status'])
                {
                    $count += 100; // 1
                } 
                if($search_array['wallet'] == $current_array['wallet'])
                {
                    $count += 100; // 2
                } 
                if($search_array['product'] == $current_array['product'])
                {
                    $count += 100; // 3
                } 
                if($search_array['channel'] == $current_array['channel'])
                {
                    $count += 100; // 4
                }
                if($search_array['terminal'] == $current_array['terminal'])
                {
                    $count += 100; // 5
                }
                if($search_array['paymentMethod'] == $current_array['paymentMethod'])
                {
                    $count += 100; // 6
                }
                if($search_array['reason'] == $current_array['reason'])
                {
                    $count += 100; // 7
                }
                if($search_array['VASCustomerAccount'] == $current_array['VASCustomerAccount'])
                {
                    $count += 100; // 8
                }
            }

        }
        $count = $count/8;

        if($count == 100){
            \Log::info("For Failed Transactions:: All identical conditions test passed!!");
        } else {
            \Log::info("For Failed Transactions:: Identical condition test broke!!");
        }

        return ($count == 100) ? true : false; 
    }

    public static function fraudulentSuccessfulTransactions($current_key, $current_array, $search_key, $search_array){
        $count = 0;
        if($search_array['status'] == "successful" and $search_array['paymentMethod'] != "card"){
            
            if($search_array['status'] == $current_array['status'])
            {
                $count += 100; // 1
            } 
            if($search_array['wallet'] == $current_array['wallet'])
            {
                $count += 100; // 2
            } 
            if($search_array['channel'] == $current_array['channel'])
            {
                $count += 100; // 3
            }
            if($search_array['terminal'] == $current_array['terminal'])
            {
                $count += 100; // 4
            }
            if($search_array['paymentMethod'] == $current_array['paymentMethod'])
            {
                $count += 100; // 5
            }
            if($search_array['reason'] == $current_array['reason'])
            {
                $count += 100; // 6
            }
            if($search_array['VASCustomerAccount'] == $current_array['VASCustomerAccount'])
            {
                $count += 100; // 7
            }

        }
        $count = $count/7;

        if($count == 100){
            \Log::info("For Successful Transactions:: All identical conditions test passed!!");
        } else {
            \Log::info("For Successful Transactions:: Identical condition test broke!!");
        }

        return ($count == 100) ? true : false; 
    }

    public static function FailedCardTransactionsWallet($current_key, $current_array, $search_key, $search_array){
        $count = 0;
        if($search_array['paymentMethod'] == "card"){
            if($search_array['status'] == "failed" or $search_array['status'] == "declined"){
                if($search_array['status'] == $current_array['status'])
                {
                    $count += 100; // 1
                } 
                if($search_array['wallet'] == $current_array['wallet'])
                {
                    $count += 100; // 2
                } 
                if($search_array['category'] == $current_array['category'])
                {
                    $count += 100; // 3
                } 
                if($search_array['channel'] == $current_array['channel'])
                {
                    $count += 100; // 4
                }
                if($search_array['terminal'] == $current_array['terminal'])
                {
                    $count += 100; // 5
                }
                if($search_array['paymentMethod'] == $current_array['paymentMethod'])
                {
                    $count += 100; // 6
                }
                if($search_array['reason'] == $current_array['reason'])
                {
                    $count += 100; // 7
                }
                if($search_array['cardPAN'] == $current_array['cardPAN'])
                {
                    $count += 100; // 8
                }
                if($search_array['cardExpiry'] == $current_array['cardExpiry'])
                {
                    $count += 100; // 9
                }
            }

        }
        $count = $count/9;

        if($count == 100){
            \Log::info("Failed Card Transactions (Wallet):: Conditions Passed!!");
        } else {
            \Log::info("Failed Card Transactions (Wallet):: Condition test broke!!");
        }

        return ($count == 100) ? true : false; 
    }

    public static function SuccessfulCardTransactionsWallet($current_key, $current_array, $search_key, $search_array){
        $count = 0;
        if($search_array['status'] == "successful" and $search_array['paymentMethod'] == "card"){
            
            if($search_array['status'] == $current_array['status'])
            {
                $count += 100; // 1
            } 
            if($search_array['wallet'] == $current_array['wallet'])
            {
                $count += 100; // 2
            } 
            if($search_array['channel'] == $current_array['channel'])
            {
                $count += 100; // 3
            }
            if($search_array['terminal'] == $current_array['terminal'])
            {
                $count += 100; // 4
            }
            if($search_array['paymentMethod'] == $current_array['paymentMethod'])
            {
                $count += 100; // 5
            }
            if($search_array['reason'] == $current_array['reason'])
            {
                $count += 100; // 6
            }
            if($search_array['VASCustomerAccount'] == $current_array['VASCustomerAccount'])
            {
                $count += 100; // 7
            }
            if($search_array['cardPAN'] == $current_array['cardPAN'])
            {
                $count += 100; // 8
            }
            if($search_array['cardExpiry'] == $current_array['cardExpiry'])
            {
                $count += 100; // 9
            }
            if($search_array['product'] == $current_array['product'])
            {
                $count += 100; // 10
            }

        }
        $count = $count/10;

        if($count == 100){
            \Log::info("Successful Card Transactions:: Test conditions passed!!");
        } else {
            \Log::info("Successful Card Transactions:: Test condtions broke!!");
        }

        return ($count == 100) ? true : false; 
    }



}