<?php

namespace App\Utils;


class RequestRules
{
    private static $rules = [
        'GEN_CREDENTIALS' => [
            'token' => 'required'
        ],

    ];

    public static function getRule($name)
    {
        return self::$rules[$name];
    }

}