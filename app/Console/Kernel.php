<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ValidateVasCommand::class,
        Commands\CustomerNotification::class,

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $filePath = "tranlog.log";
        $schedule->command('run:validate-vas')
                    ->everyFiveMinutes()
                    ->withoutOverlapping()
                    ->sendOutputTo($filePath);

        // $schedule->command('run:send-mail')
        // ->cron('*/2 * * * *')
        // ->withoutOverlapping()
        // ->sendOutputTo($filePath);
    }
}
