<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use App\Http\Controllers\VasController;
use App\Http\Controllers\CustomerNotify;
use App\Http\Controllers\VasCardController;



/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class ValidateVasCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "run:validate-vas";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Perform Validations on VAS transactions";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $vas = new VasController();
            $vasCard = new VasCardController();

            if(!file_exists("results.json")){
                $vas->getDataOnVas();
            }

            $vas->validateFailedTransactions();
            $vas->validateSuccessfulTransactions();

            $vasCard->validateFailedCardTransactions();
            $vasCard->validateSuccessfulCardTransactions();

            
            if(file_exists("results.json")){
                unlink("results.json");
            }

        } catch (Exception $e) {
            \Log::info("Error, Exception Occurred: ". $e->getMessage());
            $this->error("An error occurred: " .$e->getMessage());
        }
    }
}
            