<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use App\Http\Controllers\CustomerNotify;

/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class CustomerNotification extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "run:send-mail";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Send Payvice Receipts";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $customerNotify = new CustomerNotify();

            if(!file_exists("customers.json")){
                $customerNotify->getDataOnVas();
            }
            
            $customerNotify->prepareNotificationParams();
            
            if(file_exists("customers.json")){
                unlink("customers.json");
            }

        } catch (Exception $e) {
            \Log::info("Error, Exception Occurred: ". $e->getMessage());
            $this->error("An error occurred: " .$e->getMessage());
        }
    }
}
            